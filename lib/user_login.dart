class UserLogin {
  int id;
  String _userName;
  String _email;
  String _type;
  UserLogin(this._userName, this._email, this._type);

  void setUserName(String userName) {
    this._userName = userName;
  }

  String getUserName() {
    return _userName;
  }

  void setEmail(String email) {
    this._email = email;
  }

  String getEmail() {
    return _email;
  }

  void setType(String type) {
    this._type = type;
  }

  String getType() {
    return _type;
  }

  setId(int a) {
    id = a;
  }

  int getId() {
    return id;
  }
}
