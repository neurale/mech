import 'dart:convert';
import 'package:mech/globals.dart' as globals;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'package:mech/ui/appointment_details.dart';

import 'package:shared_preferences/shared_preferences.dart';

class CancelledJobs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CancelledJobsState();
  }
}

class _CancelledJobsState extends State<CancelledJobs> {
  bool toggle = false;
  String userId = "";
  var url = "http://test.mymech.lk//sendData.php";

  List data;
  Future<String> makeRequest() async {
    SharedPreferences.getInstance().then((prefs) {
      http.post(url, body: {
        "key": "myAppo",
        "user_id": prefs.getString('userId'),
        "subKey": "2",
      }).then((response) {
        // print(response.body);
        if (mounted) {
          setState(() {
            data = json.decode(response.body);
            toggle = true;
          });
        }
        print(response.body);
      });
    });
  }

  @override
  void initState() {
    this.makeRequest();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cancelled Services"),
      ),
      body: toggle
          ? data[0]["error"] != "Wrong"
              ? new ListView.builder(
                  itemCount: data[0]["error"] == "Wrong" ? 0 : data.length,
                  // itemExtent: 100.0,
                  itemBuilder: (BuildContext context, i) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Container(
                        // child: FittedBox(
                        child: Material(
                          color: Colors.white,
                          elevation: 14.0,
                          borderRadius: BorderRadius.circular(5.0),
                          shadowColor: Colors.grey,
                          child: ListTile(
                            title: Text(data[i]["service_name"]),
                                subtitle: Text(data[i]["vehicle_model_name"]),
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AppointmentDetails(
                                          list: data[i],
                                        ))),
                          ),
                        ),
                        // ),
                      ),
                    );
                  })
              : Container(
                  child: Center(
                    child: Text("No cancelled Services"),
                  ),
                )
          : SpinKitFadingCircle(
              color: Colors.blueAccent,
              size: 50,
            ),
      // body: Center(
      //   child: Text("SSS"),
      // ),
    );
  }
}
