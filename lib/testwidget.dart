import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Upcoming extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UpcomingState();
  }
}

class _UpcomingState extends State<Upcoming> {
  // StreamController<Photo> streamController;
  // List<Photo> list = [];
  List data = [];
  String userId = "";

  bool result = false;

  @override
  void initState() {
    // streamController = StreamController.broadcast();

    // streamController.stream.listen((p) => setState(() => list.add(p)));

    // load(streamController);

    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        userId = prefs.getString("userId");
      });
    });
    makeRequest();

    super.initState();
  }

  // load(StreamController sc) async {
  //   String url = "http://test.mymech.lk//sendData.php";
  //   var client = http.Client();
  //   // var req = http.Request('get', Uri.parse(url));
  //   var req = http.post(url, body: {
  //     "key": "myCars",
  //   }).then((response) {});
  //   print(req);
  //   // var streamResult = await client.send(req);
  // }

  bool toggle = false;

  Future<String> makeRequest() async {
    // var respose = await http.get(
    // Uri.encodeFull(url),
    // );
    String url = "http://test.mymech.lk//sendData.php";

    http.post(url, body: {
      "key": "showAppointment",
      "userId": userId,
      "type": "M"
    }).then((respose) {
      if (this.mounted) {
        setState(() {
          data = json.decode(respose.body);
          toggle = true;
        });
      }
      if (data[0]["error"] != "error") {
        if (this.mounted) {
          setState(() {
            result = true;
          });
        }
      }
      print(respose.body);
    });
  }

  // Widget _makeElement(int index) {
  //   // if (index >= list.length) return null;
  //   return ListTile(
  //       contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
  //       leading: Container(
  //         padding: EdgeInsets.only(right: 12.0),
  //         decoration: new BoxDecoration(
  //             border: new Border(
  //                 right: new BorderSide(width: 1.0, color: Colors.white24))),
  //         child: Icon(Icons.build, color: Colors.blue),
  //       ),
  //       title: Text(
  //         "JOB ID :  5",
  //         style:
  //             TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.bold),
  //       ),
  //       // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

  //       subtitle: Row(
  //         children: <Widget>[
  //           ButtonTheme.bar(
  //             // make buttons use the appropriate styles for cards
  //             child: ButtonBar(
  //               children: <Widget>[
  //                 FlatButton(
  //                   child: const Text('ACCEPT'),
  //                   textColor: Colors.blueGrey,
  //                   onPressed: () {/* ... */},
  //                 ),
  //                 FlatButton(
  //                   child: const Text('REJECT'),
  //                   textColor: Colors.blueGrey,
  //                   onPressed: () {/* ... */},
  //                 ),
  //               ],
  //             ),
  //           ),
  //         ],
  //       ),
  //       trailing:
  //           Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0));
  // }

  Stream _streamRequest() async* {
    String url = "http://test.mymech.lk//sendData.php";

    yield* Stream.periodic(Duration(seconds: 10), (int a) {
      print(a++);

      makeRequest();

      // http.post(url, body: {
      //   "key": "showAppointment",
      //   "userId": "3",
      //   "type":"M"
      // }).then((respose) {
      //   setState(() {
      //     data = json.decode(respose.body);
      //   });
      //   // print(data[0]['brand_name']);
      //   print(respose.body);
      // });
    });
  }

  @override
  Widget build(BuildContext context) {
    makeRequest();
    return result
        ? StreamBuilder(
            initialData: data,
            stream: _streamRequest(),
            builder: (context, snapshot) {
              return Scaffold(
                body: toggle
                    ? ListView.builder(
                        itemCount: !result ? 0 : data.length,
                        itemBuilder: (context, i) {
                          // return Text(data[i]['brand_name']);
                          return Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Container(
                              // child: FittedBox(
                              child: Material(
                                color: Colors.white,
                                elevation: 14.0,
                                borderRadius: BorderRadius.circular(5.0),
                                shadowColor: Colors.grey,
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      // color: Colors.amber,
                                      width: 250,
                                      child: Column(
                                        children: <Widget>[
                                          Text(data[i]["vehicle_details"]),
                                          // Text(data[i]["vehicle_details"][0]["service_name"]()),
                                          // Divider(
                                          //   color: Colors.black,
                                          // ),
                                          // FittedBox(
                                          //   child: Row(
                                          //     children: <Widget>[
                                                // FlatButton(
                                                //   color: Colors.greenAccent,
                                                //   child: Text("Accept"),
                                                //   onPressed: () {},
                                                // ),
                                                // SizedBox(width: 100,),
                                                // FlatButton(
                                                //   color: Colors.redAccent,
                                                //   child: Text("Reject"),
                                                //   onPressed: () {},
                                                // ),
                                          //     ],
                                          //   ),
                                          // )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      child: FlatButton(
                                        color: Colors.redAccent,
                                        child: Text("Reject"),
                                        onPressed: () {},
                                      ),
                                      //   width: 100,
                                      //   height: 100,
                                      //   child: ClipRRect(
                                      //       borderRadius: BorderRadius.circular(24.0),
                                      //       child: Image(
                                      //         fit: BoxFit.contain,
                                      //         alignment: Alignment.topRight,
                                      //         image: NetworkImage(
                                      //             "https://cdn3.vectorstock.com/i/1000x1000/69/32/spanner-wrench-mechanic-tool-vector-12156932.jpg"),
                                      //       )),
                                    ),
                                  ],
                                ),
                              ),
                              // ),
                            ),
                          );
                        },
                      )
                    : SpinKitCircle(
                        color: Colors.blue,
                      ),
              );
            })
        : Container(
            child: Center(
              child: Text("You dont have any job request"),
            ),
          );
  }
}
