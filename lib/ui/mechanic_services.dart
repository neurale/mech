import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:mech/ui/appointment_details.dart';
import 'package:mech/ui/dashboard.dart';
import 'package:mech/upcoming_jobs.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MechanicServices extends StatefulWidget {
  String appoId;

  MechanicServices({this.appoId});

  @override
  State<StatefulWidget> createState() {
    return _MechanicServicesState();
  }
}

class _MechanicServicesState extends State<MechanicServices> {
  String _serType;
  bool _emergencySelected = false;
  bool _mainteneceSelected = false;
  bool _spaSelected = false;
  bool _inspectionSelected = false;
  int groupValue;
  bool toggle = false;
  bool carToggle = false;

  String selected = null;

  var currentLocation;
  int _visitTypeSelected = 1;
  double lat;
  double lan;

  int dat;
  int _serviceType = 1;

  void radionFucntion(int radioindex) {
    _mainteneceSelected = false;
    _emergencySelected = false;
    _spaSelected = false;
    _inspectionSelected = false;
    setState(() {
      switch (radioindex) {
        case 1:
          _emergencySelected = true;
          break;
        case 2:
          _mainteneceSelected = true;
          break;
        case 3:
          _spaSelected = true;
          break;
        case 4:
          _inspectionSelected = true;
          break;
      }
    });
  }

  var url = "http://test.mymech.lk//sendData.php";
  List mechService;
  Future<String> subService(int typeKey) async {
    await http.post(url, body: {
      "key": "subService",
      "id": typeKey.toString(),
    }).then((respose) {
      setState(() {
        mechService = json.decode(respose.body);
        toggle = true;
      });
      print(respose.body);
    });
  }

  Future<void> _neverSatisfied(message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
            title: Text(message),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Press OK to continue'),
                  //Text('You\’re like me. I’m never satisfied.'),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  //
                  //openSettingsMenu("ACTION_SETTINGS");
                  Navigator.pop(context);
                  Navigator.pop(context);
                  // _Satisfied();
                  // Navigator.pushReplacement(context,
                  //     MaterialPageRoute(builder: (context) => Upcoming()));
                  // Navigator.pushNamedAndRemoveUntil(
                  //     context, '/dashboard', (_) => false);
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Future<String> addSubSerivce(String serviceId) async {
    await http.post(url, body: {
      "key": "add_service",
      "app_id": widget.appoId,
      "vs_id": serviceId,
      "s_type": _serviceType.toString(),
    }).then((respose) {
      print(respose.body);
      List result = json.decode(respose.body);
      if (result[0]["error"] == "Wrong") {
        _neverSatisfied("Ooops! Network Error");
      } else {
        _neverSatisfied("Successfully Added");
      }
    });
  }

  bool editable = false;

  String service;

  @override
  void initState() {
    subService(1);
    _serType = "Emergency";
    _emergencySelected = true;
    super.initState();
  }

  Widget serviceType(String type, int typeKey) {
    return Padding(
      padding: EdgeInsets.only(left: 2.0, top: 10.0),
      child: InkWell(
        onTap: () {
          setState(() {
            _serType = type;
          });
          subService(typeKey);
        },
        child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.circular(50.0),
            child: Container(
              height: 100.0,
              width: 125.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50.0),
                color: Colors.blueGrey,
              ),
              child: Center(
                child: Text(
                  type,
                  style: TextStyle(color: Colors.white),
                ),
                // child: Image.asset("breakdedit"),
              ),
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(0.0),
            child: Container(
              child: Center(
                child: Icon(Icons.more_horiz),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            alignment: FractionalOffset.topCenter,
            height: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 2.0, top: 10.0),
                  child: InkWell(
                    onTap: () {
                      radionFucntion(1);
                      setState(() {
                        _serType = "Emergency";
                        _serviceType = 1;
                      });

                      subService(1);
                    },
                    child: Material(
                        elevation: 4.0,
                        child: Container(
                          height: 100.0,
                          width: 125.0,
                          decoration: BoxDecoration(
                            color: _emergencySelected
                                ? Colors.blue
                                : Colors.blueGrey,
                          ),
                          child: Center(
                            child: Image(
                              image: AssetImage("assets/breakDown.JPG"),
                              fit: BoxFit.fill,
                            ),
                          ),
                        )),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 2.0, top: 10.0),
                  child: InkWell(
                    onTap: () {
                      radionFucntion(2);
                      setState(() {
                        _serType = "Mainterence";
                        _serviceType = 2;
                      });
                      subService(2);
                    },
                    child: Material(
                        elevation: 4.0,
                        child: Container(
                          height: 100.0,
                          width: 125.0,
                          decoration: BoxDecoration(
                            color: _mainteneceSelected
                                ? Colors.blue
                                : Colors.blueGrey,
                          ),
                          child: Center(
                            child: Image(
                              image: AssetImage("assets/maintenence.JPG"),
                              fit: BoxFit.fill,
                            ),
                            // child: Image.asset("breakdedit"),
                          ),
                        )),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 2.0, top: 10.0),
                  child: InkWell(
                    onTap: () {
                      radionFucntion(3);
                      setState(() {
                        _serType = "Car Spa";
                        _serviceType = 3;
                      });
                      subService(3);
                    },
                    child: Material(
                        elevation: 4.0,
                        // borderRadius: BorderRadius.circular(50.0),
                        child: Container(
                          height: 100.0,
                          width: 125.0,
                          decoration: BoxDecoration(
                            // borderRadius: BorderRadius.circular(50.0),
                            color: _spaSelected ? Colors.blue : Colors.blueGrey,
                          ),
                          child: Center(
                            child: Image(
                              image: AssetImage("assets/carSpa.JPG"),
                              fit: BoxFit.fill,
                            ),
                            // child: Image.asset("breakdedit"),
                          ),
                        )),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 2.0, top: 10.0),
                  child: InkWell(
                    onTap: () {
                      radionFucntion(4);
                      setState(() {
                        _serType = "Pre Inspection";
                        _serviceType = 4;
                      });
                      subService(3);
                    },
                    child: Material(
                        elevation: 4.0,
                        // borderRadius: BorderRadius.circular(50.0),
                        child: Container(
                          height: 100.0,
                          width: 125.0,
                          decoration: BoxDecoration(
                            // borderRadius: BorderRadius.circular(50.0),
                            color: _inspectionSelected
                                ? Colors.blue
                                : Colors.blueGrey,
                          ),
                          child: Center(
                            child: Image(
                              image: AssetImage("assets/inspection.JPG"),
                              fit: BoxFit.fill,
                            ),
                            // child: Image.asset("breakdedit"),
                          ),
                        )),
                  ),
                ),
              ],
            ),
          ),
          Divider(),
          Expanded(
            // height: 500,
            child: Container(
              child: Scrollbar(
                child: toggle
                    ? ListView.builder(
                        itemCount: mechService == null ? 0 : mechService.length,
                        itemBuilder: (context, i) {
                          // return Text(data[i]['brand_name']);
                          return Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Container(
                              // child: FittedBox(
                              child: Material(
                                color: Colors.white,
                                elevation: 14.0,
                                borderRadius: BorderRadius.circular(5.0),
                                shadowColor: Colors.grey,
                                child: ListTile(
                                    title: Text(
                                      mechService[i]["service_name"],
                                      style: TextStyle(fontSize: 20),
                                    ),
                                    // subtitle: Text(
                                    //     mechService[i]["service_description"]),
                                    onTap: () {
                                      addSubSerivce(
                                          mechService[i]["service_id"]);
                                    }),
                              ),
                            ),
                          );
                        },
                      )
                    : SpinKitFadingCircle(
                        color: Colors.blueAccent,
                        size: 50,
                      ),
              ),
            ),
          ),
          Divider(),
        ],
      ),
    );
  }

  void select(e) {
    setState(() {
      if (e == 1)
        groupValue = 1;
      else if (e == 2)
        groupValue = 2;
      else if (e == 3)
        groupValue = 3;
      else if (e == 4) groupValue = 4;
    });
  }
}
